$(window).scroll(function() {
    if ($(document).scrollTop() < 500) {
        $('#首页导航').css({
            'fill':'#ffffff'
        })
        $('.navbar').css({
            'background-color': 'rgba(45, 41, 41, 0.6)',
            'color': '#ffffff',
            'box-shadow':'0 0 0 #DDDDDD'
        })
        $('#search').attr('src', '../icons/search.svg');
        $('#navbarListButton').attr('src', '../icons/menu.svg');
    } else {
        $('#首页导航').css({
            'fill':'#333333'
        })
        $('.navbar').css({
            'background-color': '#ffffff',
            'color': '#000000',
            'box-shadow':'0 2px 7px #DDDDDD'
        })
        $('#search').attr('src', '../icons/search-24px.svg');
        $('#navbarListButton').attr('src', '../icons/menu-24px.svg');
    }
});