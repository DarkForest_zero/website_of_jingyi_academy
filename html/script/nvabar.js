$(window).scroll(function() {
    if ($(document).scrollTop() < 500) {
        $('nav').addClass('shrink');
        $('#logo').attr('src', '../Logo/logo.svg');
        $('#search').attr('src', '../icons/search.svg');
        $('#logo').css({
            'height': '80px',
            'width': '',
        })
        $('.navbar').css({
            'background-color': 'rgba(45, 41, 41, 0.6)',
            'color': '#ffffff',
            'box-shadow': '0 0 0 #DDDDDD',
        })
        $('.navlist').css({
            'margin-left': '2px',
            'padding-top': '4px',
            'padding-bottom': '4px',
            'width': '80px',
            'color': '#ffffff',
        })
        $('.nav').css({
            'float': 'right'
        })
        $('.navbox').css({
            'width': '891px'
        })
        // $('.container').css({
        //     'padding-top': '20px',
        //     'padding-bottom': '20px'
        // })
        $('.navspace').css({
            'width': '96px'
        })
        document.getElementsByClassName('navlist').color='#FFFFFF'
        // $('a').css({
        //     'color': '#ffffff'
        // })
    } else {
        $('nav').removeClass('shrink');
        $('#logo').attr('src', '../Logo/logo_sm.svg');
        $('#search').attr('src', '../icons/search-24px.svg');
        $('#logo').css({
            'height': '30px',
            // 'width': '80px',
            // 'text-align': 'center'
        })
        $('.navbar').css({
            'background-color': '#ffffff',
            'box-shadow': '0 2px 7px #DDDDDD',
            'color': '#000000',
        })
        $('.navlist').css({
            'margin-left': '20px',
            'padding-top': '4px',
            'padding-bottom': '4px',
            'width': '80px',
            'color': '#000000',
        })
        $('.nav').css({
            'float': 'left'
        })
        $('.navbox').css({
            'width': '1070px'
        })
        // $('.container').css({
        //     'padding-top': '10px',
        //     'padding-bottom': '15px'
        // })
        $('.navspace').css({
            'width': '140px'
        })
        document.getElementsByClassName('navlist').color='#000000'
        // document.getElementsByTagName('a').color='#000000'
        // $('a').css('color','#000000')    
    }
});