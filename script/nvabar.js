$(window).scroll(function () {
    if ($(document).scrollTop() < 500) {
        $('nav').addClass('shrink');
        $('#logo').attr('src', './Logo/logo.svg');
        $('#search').attr('src', './icons/search.svg');
        $('#logo').css({
            'height': '80px',
        })
        $('.navbar').css({
            'background-color': 'rgba(45, 41, 41, 0.6)',
            'color': '#ffffff',
            'box-shadow':'0 0 0 #DDDDDD'
        })
        $('.navlist').css({
            'margin-left': '2px',
            'padding-top':'4px',
            'padding-bottom':'4px',
            'width':'80px',
        })
        $('.nav').css({
            'float':'right'
        })
        $('.navbox').css({
            'width':'891px'
        })
        $('.navspace').css({
            'width': '96px'
        })
        document.getElementsByClassName('navlist').color='#FFFFFF'

    } else {
        $('nav').removeClass('shrink');
        $('#logo').attr('src', './Logo/logo_sm.svg');
        $('#search').attr('src', './icons/search-24px.svg');
        $('#logo').css({
            'height': '30px',
        })
        $('.navbar').css({
            'background-color': '#ffffff',
            'color': '#000000',
            'box-shadow':'0 2px 7px #DDDDDD'
        })
        $('.navlist').css({
            'margin-left': '20px',
            'padding-top':'4px',
            'padding-bottom':'4px',
            'width':'80px'
        })
        $('.nav').css({
            'float':'left'
        })
        $('.navbox').css({
            'width':'1070px'
        })
        // $('.container').css({
        //     'padding-top': '10px',
        //     'padding-bottom': '15px'
        // })
        $('.navspace').css({
            'width': '140px'
        })
        document.getElementsByClassName('navlist').color='#000000'
        
    }
});